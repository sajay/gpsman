#![allow(unused_imports)]
#![allow(unreachable_code)]
#![allow(unused_variables)]
#![allow(unused_assignments)]
#![allow(dead_code)]

use std::{env, io::{BufReader, BufRead, Write}, fs::{File} };
use std::process::Command;
use std::fs::OpenOptions;
mod objects;
use crate::objects::*;
mod compress_cell_db;
mod cleangpx;
mod export_route;
mod list_cached_cells;
mod export_to_device;
mod export_manifest;

fn main() {
	set_cache_dir();

	let cell_cache_location: String = format!("{}{}", get_cache_dir(), "cell_profiles/");

	let args: Vec<String> = env::args().collect();
	
	if args.len() < 2 { println!("Invalid usage.\nHelp:\n{0}", HELP_DESC); std::process::exit(1) }
	
	match args[1].as_str() {
		"CacheCellTowers" => {
			let mut towers_file: String = "".to_string();
			let mut radio: String = "LTE".to_string();
			let mut mcc: Vec::<u16> = vec!{310,311,312,316};
			let mut mnc: Vec::<u16> = vec!{240,660,230,31,220,270,210,260,200,250,160,300,280,330,800,310};
			let mut filter_radius: u16 = 3;
				
			for arg in args.iter().skip(2) {
				if !arg.starts_with("--") || !arg.contains("=") { println!("Invalid arg: {}", arg); std::process::exit(1) }
			
				let arg_key: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[0].to_string();
				let arg_val: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[1].to_string();
				match arg_key.as_str() {
					"TowersFile" => towers_file = arg_val,
					"radio" => radio = arg_val,
					"MCC" => mcc = arg_val.split(",").map(|x| 
						match x.parse::<u16>() {
							Ok(val) => return val,
							_ => { println!("MCC must be comma delimited 16bit integers."); std::process::exit(1) }
						}).collect::<Vec<_>>(),
					"MNC" => mnc = arg_val.split(",").map(|x| 
						match x.parse::<u16>() {
							Ok(val) => return val,
							_ => { println!("MNC must be comma delimited 16bit integers."); std::process::exit(1) }
						}).collect::<Vec<_>>(),
					"FilterRadius" => match arg_val.parse::<u16>() {
							Ok(val) => filter_radius = val,
							_ => { println!("Filter radius must be an unsigned 16bit integer."); std::process::exit(1); }
						},
					_ => { println!("Unknown arg: {}", arg); std::process::exit(1) }
				}
			}
				
			if towers_file.trim().is_empty() { panic!("Parameter --TowersFiles=<file path to opencellid> required."); }
			
			let file = File::open(&towers_file).expect(format!("Failed to open input file: {}", towers_file).as_str());
		  
			let reader = BufReader::new(file);

			let ofile_name: &String = &format!("{}{}", cell_cache_location, towers_file
				.split("/").collect::<Vec<_>>()
				.last().expect("tower file failed to split on dir /")
				.split(".").collect::<Vec<_>>()
				.first().expect("tower file name failed to split on ."));

			let mut ofile = OpenOptions::new()
						.write(true)
                        .create(true)
                        .truncate(true)
                        .open(&ofile_name)
                        .expect(format!("Failed to open output file: {}", ofile_name).as_str());
			ofile.write_all(b"\n").expect(format!("Failed to write tower into cache file. {}", ofile_name).as_str());

			for t in compress_cell_db::compress_cell_db(towers_file.as_str(), mnc, mcc, filter_radius.into()) {
				ofile.write_all(format!("{},{},{}\n", t.lat, t.lon, t.samples).as_bytes()).expect(format!("Failed to write tower into cache file. {}", ofile_name).as_str());
			}
		}
		"ListCachedCells" => {
			for arg in args.iter().skip(2) {
				println!("Unknown arg: {}", arg); std::process::exit(1);
			}
			
			println!("Available Cell Profiles:\n\t{}", list_cached_cells::list_cached_cells().join("\n\t"));
			return;
		}
		"ExportCellTowers" => {
			let mut cell_profile: String = "".to_string();
			let mut output_file: String = "".to_string();
			
			for arg in args.iter().skip(2) {
				if !arg.starts_with("--") || !arg.contains("=") { println!("Invalid arg: {}", arg); std::process::exit(1) }
			
				let arg_key: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[0].to_string();
				let arg_val: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[1].to_string();
				
				match arg_key.as_str() {
					"cellFile" => cell_profile = arg_val,
					"outputFile" => output_file = arg_val,
					_ => { println!("Unknown arg: {}", arg); std::process::exit(1) }
				}
			}
				
			if cell_profile.trim().is_empty() { println!("Argument required: cell-file"); std::process::exit(1); }
			if output_file.trim().is_empty() { println!("Argument required: output-file"); std::process::exit(1); }
			
			let mut ofile = File::create(&output_file).expect(format!("Failed to open output file: {}", output_file).as_str());
	
 			ofile.write_all(b"<gpx>\n").expect(format!("{}{}", "Failed to write <gpx> to file: ", output_file).as_str());

 			let ifile_name: String = format!("{}{}", cell_cache_location, cell_profile);

 			let towers = compress_cell_db::load_cell_profile(&ifile_name.as_str());

 			for tmp in towers {
				ofile.write_all(
					format!("<wpt lat=\"{}\" lon=\"{}\"><cmt>Samples: {}</cmt></wpt>\n", 
					tmp.lat, 
					tmp.lon, 
					tmp.samples)
					.as_bytes()
					).expect(format!("{}{}", "Failed to write towerline to file: ", output_file).as_str());
			}
    		ofile.write_all(b"</gpx>\n").expect(format!("{}{}", "Failed to write </gpx> to file: ", output_file).as_str());
		}
		"CleanGPX" => {
			let mut gpx_file: String = "".to_string();

			for arg in args.iter().skip(2) {
				if !arg.starts_with("--") || !arg.contains("=") { println!("Invalid arg: {}", arg); std::process::exit(1); }
				
				let arg_key: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[0].to_string();
				let arg_val: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[1].to_string();
				match arg_key.as_str() {
					"gpxFile" => gpx_file = arg_val,
					_ => { println!("Unknown arg: {}", arg); std::process::exit(1) }
				}
			}
			
			if gpx_file.trim().is_empty() { println!("Argument required: gpx-input-file"); std::process::exit(1); }

			cleangpx::cleangpx(&gpx_file);
		}
                "ExportManifest" => {
                        let mut gpx_input_file: String = "ddda".to_string();
                        let mut cell_file: String = "".to_string();
                        let mut manifest_output_file: String = "".to_string();

                        for arg in args.iter().skip(2) {
                                if !arg.starts_with("--") || !arg.contains("=") { println!("Invalid arg: {}", arg); std::process::exit(1); }

                                let arg_key: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[0].to_string();
                                let arg_val: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[1].to_string();
                                match arg_key.as_str() {
                                        "gpx-input-file" => gpx_input_file = arg_val,
                                        "cell-profile" => cell_file = arg_val,
                                        "manifest-output-file" => manifest_output_file = arg_val,
                                        _ => { println!("Unknown arg: {}", arg); std::process::exit(1) }
                                }
                        }

                        if gpx_input_file.trim().is_empty() { println!("Argument required: gpx-input-file"); std::process::exit(1); }
                        let towers: Vec::<Tower> =
                                if cell_file.trim().is_empty() {
                                        Vec::<Tower>::new()
                                } else {
                                        compress_cell_db::load_cell_profile(format!("{}{}", cell_cache_location, cell_file).as_str())
                                };

                        export_manifest::export_manifest(
                                gpx_input_file.as_str(),
                                towers,
                                manifest_output_file.as_str()
                                );
                }
		"ExportRoute" => {
			let mut gpx_input_file: String = "ddda".to_string();
			let mut routino_profile: String = "bicycle".to_string();
			let mut cell_file: String = "".to_string();
			let mut gpx_output_file: String = "".to_string();
			let mut manifest_output_file: String = "".to_string();
			let mut routino_db: String = "".to_string();
			
			for arg in args.iter().skip(2) {
				if !arg.starts_with("--") || !arg.contains("=") { println!("Invalid arg: {}", arg); std::process::exit(1); }
				
				let arg_key: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[0].to_string();
				let arg_val: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[1].to_string();
				match arg_key.as_str() {
					"gpx-input-file" => gpx_input_file = arg_val,
					"routino-profile" => routino_profile = arg_val,
					"cell-profile" => cell_file = arg_val,
					"gpx-output-file" => gpx_output_file = arg_val,
					"manifest-output-file" => manifest_output_file = arg_val,
					"routino-db" => routino_db = arg_val,
					_ => { println!("Unknown arg: {}", arg); std::process::exit(1) }
				}
			}
			
			if gpx_input_file.trim().is_empty() { println!("Argument required: gpx-input-file"); std::process::exit(1); }
			if routino_db.trim().is_empty() { println!("Argument required: routino-db"); std::process::exit(1); }
			if !routino_db.ends_with("-nodes.mem") { println!("Routino requires database file to end in \"-nodes.mem\""); std::process::exit(1); }
			let towers: Vec::<Tower> = 
				if cell_file.trim().is_empty() { 
					Vec::<Tower>::new()
				} else {
					compress_cell_db::load_cell_profile(format!("{}{}", cell_cache_location, cell_file).as_str())
				};

			export_route::export_route(
				gpx_input_file.as_str(), 
				routino_profile.as_str(), 
				routino_db.as_str(),
				towers, 
				gpx_output_file.as_str(), 
				manifest_output_file.as_str()
				);
		}
		"ExportToDevice" => {
			let mut gpx_input_file: String = "".to_string();
			let mut export_dir: String = "./output".to_string();
			let mut extention: String = ".xml".to_string();
			
			for arg in args.iter().skip(2) {
				if !arg.starts_with("--") || !arg.contains("=") { println!("Invalid arg: {}", arg); std::process::exit(1); }
				
				let arg_key: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[0].to_string();
				let arg_val: String = arg.chars().skip(2).collect::<String>().split("=").collect::<Vec<_>>()[1].to_string();
				match arg_key.as_str() {
					"gpx-input-file" => { gpx_input_file = arg_val },
					"export-dir" => export_dir = arg_val,
					"extention" => extention = arg_val,
					_ => { println!("Unknown arg: {}", arg); std::process::exit(1) }
				}
			}
			
			if gpx_input_file.trim().is_empty() { println!("Argument required: gpx-input-file"); std::process::exit(1); }

			export_to_device::export_route_to_device(gpx_input_file.as_str(), export_dir.as_str(), extention.as_str());
		}
		_ => { println!("Invalid usage, unknown function: {}\nHelp:\n{}", args[1], HELP_DESC); std::process::exit(1); }
	}
}

const HELP_DESC: &str = "This utility includes functions assisting with the creation of routes for gps-computers, specificly the wahoo elmt. Typical workflow is caching and exporting cell towers, for use in QMapShack. After creating gpx file with QMapShack, generate routes and manifest here.
	Usage:
		gpxman <function> [function specific params]
	Functions:
		CacheCellTowers:
			Searches given cell database and stores it into cache. This function can take a long time to process.
			--TowersFile=<path to opencellid .csv file> We only accept the cell tower database that can be found for free at opencellid.org
			[--radio=<radio>] Default=\"LTE\" Ex. LTE/GSM/CDMA. Simple string match for tower's radio.
			[--MCC=<mobile country code>] Default=310,311... (US codes default)
			[--MNC=<mobile network codes>] Default=240,660... (T-Mobile MNC is default)
			[--FilterRadius=<radius>] Default=3 Radius in kilometers that other towers will not be placed, to avoid near duplicates. This has a significant effect on performance and probably shouldn't be turned lower.
		ListCachedCells:
			Prints cache cell files that have been previously computed and cached. Cell files are available for manual interaction at ~/.gpxman.cache/cell-profiles
		ExportCellTowers:
			Exports cell profile into a gpx file that can be imported to map editors.
			--cellFile=<cached cell file> Name of cell profile to convert to gpx.
			--outputFile=<path to new gpx file>
		CleanGPX:
			Removes anything that isn't a route, track or waypoint from a GPX file. This is specificly for cleaning QMapShack's useless history and metadata.
			--gpxFile=<path to gpx file>
		ExportRoute:
			Takes a GPX file filed with waypoints, and creates routes between them, based on the order they are in the file. Output can be the same file, or a new file. When the same file is used, routes will be cleared and rebuilt, waypoints left alone. If a new file is specified, no waypoints will be added, only routes.
			--gpx-input-file=<path to input gpx file> File that waypoints are taken from. Any routes/tracks in this file are ignored.
			--routino-db=<db file> Use routino to generate this database. Must include area around all waypoints.
			[--routino-profile=<routino-profile>] Default=\"Bicycle\" Routino profile to use for routing.
			[--cell-profile=<filename of cached cell profile>] Using value default will pull first cell file found in cache. Leaving blank will result in no cell data being calculated.			
			[--gpx-output-file=<path to output gpx file>] If parameter is unused, no gpx data will be generated anywhere. If output matches input, only the routes section will be updated. If value is stdout, it will be printed to console.
			[--manifest-output-file=<path to output manifest file>] Manifest file is plain text document used for planing that includes various info about each route in human readable format. If value it stdout, is will be printed to console.
                ExportManifest:
                        Takes a GPX file filed with waypoints and routes, and creates a test file based on the order they are in the file.
                        --gpx-input-file=<path to input gpx file> File that waypoints are taken from. Any routes/tracks in this file are ignored.
                        [--cell-profile=<filename of cached cell profile>] Using value default will pull first cell file found in cache. Leaving blank will result in no cell data being calculated.
                        [--manifest-output-file=<path to output manifest file>] Manifest file is plain text document used for planing that includes various info about each route in human readable format. If value it stdout, is will be printed to console.
		ExportToDevice:
			Takes a gpx file of routes, and exports routes in individual files to a directory.
			--gpx-input-file=<path to input gpx file> File that waypoints are taken from. Any routes/tracks that are in the file are not used to calculate output routes.
			--export-dir=<path to output directory> All output files will be created here.
			--extention=<fileextention>] Default=\".xml\" Extention of files. Some devices (Ie. The Wahoo Elmt) only recognize .xml files.
	";

fn set_cache_dir() {
		if cfg!(target_os = "windows") {
		panic!("I don't know how to create dir on windows.");
		//Command::new("cmd").args(["/C", "echo hello"]).output().expect("failed to execute process")
	} else {
		Command::new("sh").arg("-c").arg("mkdir -p ~/.gpxman.cache").spawn().expect("Failed to create gpxman cache file. CMD: mkdir -p ~/gpxman.cache");
	};
	
	if cfg!(target_os = "windows") {
		panic!("I don't know how to create dir on windows.");
		//Command::new("cmd").args(["/C", "echo hello"]).output().expect("failed to execute process")
	} else {
		Command::new("sh").arg("-c").arg("mkdir -p ~/.gpxman.cache/cell_profiles").spawn().expect("Failed to create cell_profile dir under gpxman cache. CMD: mkdir -p ~/gpxman.cache/cell_profiles");
	};
}

fn get_cache_dir() -> String {
	let output: std::process::Output = if cfg!(target_os = "windows") {
		panic!("I don't know how to create dir on windows.");
		//Command::new("cmd").args(["/C", "echo hello"]).output().expect("failed to execute process")
	} else {
		Command::new("sh").arg("-c").arg("echo ~/.gpxman.cache/").output().expect("Failed to expand cache location. CMD: echo ~/.gpxman.cache/")
	};

	return std::str::from_utf8(&output.stdout).expect("Failed to convert output of command.").to_string().replace("\n", "");
}
