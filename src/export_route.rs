use rayon::prelude::*;
use crate::objects::*;
use std::io::BufReader;
use std::fs::File;
use std::{env, io::{BufRead, Write}, fs };
use xmltree::{Element, XMLNode};
use std::process::Command;
use crate::compress_cell_db;

pub fn export_route(gpx_in: &str, routino_profile: &str, routino_db: &str, towers: Vec::<Tower>, gpx_out: &str, manifest_out: &str) {
	let mut waypoints = Vec::<Waypoint>::new();
	let mut routes = Vec::<Route>::new();

    let ifile = fs::read_to_string(gpx_in).expect(format!("Failed to open gpx route file for reading: {}", gpx_in).as_str());
	let mut root = Element::parse(ifile.as_bytes()).unwrap();
 
	waypoints = root.children.iter()
		.filter_map(|child| { 
			match child {
				XMLNode::Element(e) => 
					if e.name == "wpt" {
						return Some(e)
					} else {
				 		None
				 	}
				_ => None
			}
		})
		.map(|e| {
			let name: Option::<String> = (|| {
				Some(e.children.iter().find_map(|child|{
					if child.as_element()?.name == "name" {
						return Some(child.as_element()?);
					}
					None
				})?
				.children.iter().find_map(|child|{ Some(child.as_text()?) })?
				.to_string())
			})();

			let cmt: Option::<String> = (|| {
				Some(e.children.iter().find_map(|child|{
					if child.as_element()?.name == "cmt" {
						return Some(child.as_element()?);
					}
					None
				})?
				.children.iter().find_map(|child|{ Some(child.as_text()?) })?
				.to_string())
			})();

			let desc: Option::<String> = (|| {
				Some(e.children.iter().find_map(|child| {
					if child.as_element()?.name == "desc" {
						return Some(child.as_element()?);
					}
					None
				})?
				.children.iter().find_map(|child|{ Some(child.as_text()?) })?
				.to_string())
			})();

			Waypoint {
    			lat: e.attributes["lat"].parse::<f32>()
    				.expect("Lat values in waypoint not a float."),
				lon: e.attributes["lon"].parse::<f32>()
    				.expect("Lat values in waypoint not a float."),
    			desc: format!("{}{}",
    				if let Some(i) = cmt {i} else { "".to_string() },
    				if let Some(i) = desc {i} else { "".to_string() }
    				),
    			name: if let Some(i) = name {i} else { "".to_string() },
    			nearest_tower: 0.0
        	}
		})
		.collect::<Vec<_>>();

	waypoints[0..waypoints.len() - 1].iter().enumerate().for_each(|(i, x)| {
		routes.push(
			route(&waypoints[i], &waypoints[i+1], routino_profile, routino_db, &towers)
			);
		println!("Calculated route {} of {}", routes.len(), waypoints.len() - 1);
	});

	if !gpx_out.trim().is_empty() {
		if gpx_out == gpx_in {
			root.children = root.children.iter().filter(|node| {
				match node {
					XMLNode::Element(e) => {
						e.name != "rte"
					},
					_ => true
				}
			})
			.map(|x|x.clone())
			.collect::<Vec<_>>();
		}
		else {
			root = Element::new("gpx");
		}

		routes.iter().for_each(|r|{
			let mut route_element = Element::new("rte");
			let mut name_element = Element::new("name");
			name_element.children.push(XMLNode::Text(r.name.clone()));
			route_element.children.push(xmltree::XMLNode::Element(name_element));
			let mut cmt_element = Element::new("cmt");
			cmt_element.children.push(XMLNode::Text(format!(
				"Length: {}<br>Cell Coverage: {:.0}%<br>{}",
				r.length / 1.60934,
				r.cell_coverage * 100.0,
				match r.waypoints.last() {
					Some(e) => if f32::MAX == e.nearest_tower { 
							"".to_string()
						} else { 
							format!("Endpoint dist to cell: {:.2}mi", e.nearest_tower / 1.60934) 
						},
					None => "0.0".to_string()
				}
				)));
			route_element.children.push(xmltree::XMLNode::Element(cmt_element));

			r.waypoints.iter().for_each(|w| {
				let mut w_element = Element::new("rtept");
				w_element.attributes.insert("lat".to_string(), w.lat.to_string());
				w_element.attributes.insert("lon".to_string(), w.lon.to_string());
				route_element.children.push(xmltree::XMLNode::Element(w_element));
			});

			root.children.push(xmltree::XMLNode::Element(route_element));
		});

		if gpx_out == gpx_in {
			std::fs::rename(gpx_in, format!("{}.bak", gpx_in)).expect(format!("Failed to backup file: {}", gpx_in).as_str());
		}
	 	root.write_with_config(
	 		fs::File::create(gpx_out).expect(format!("Failed to write clean xml to gpx file. {}", gpx_out).as_str()),
	 		xmltree::EmitterConfig::new().line_separator("\n").perform_indent(true)
	 		).expect(format!("Failed to write clean xml to gpx file. {}", gpx_out).as_str());
	 	if gpx_out == gpx_in {
	 		std::fs::remove_file(format!("{}.bak", gpx_in)).expect(format!("Failed to backup file: {}", gpx_in).as_str());
	 	}

	}


	if !manifest_out.trim().is_empty() {
		let mut ofile_mani = File::create(&manifest_out)
			.expect(format!("Failed to open output manifest_out file: {}", manifest_out).as_str());

		ofile_mani.write_all(b"Sequence\tEnd Waypoint\tCell Coverage\tEndpoint cell dist (mi)\tLength (mi)\n")
			.expect(format!("Failed to header to manifest file. {}", manifest_out).as_str());

		routes.iter().enumerate().for_each(|(index, route)| {
			ofile_mani
				.write_all(format!("{}\t{}\t{:.0}%\t{:.2}\t{:.2}\n",
					index,
					route.waypoints.last().expect("Route has no waypoints. Can't export").name,
					route.cell_coverage*100.0,
					route.waypoints.last().expect("Route has no waypoints. Can't export").nearest_tower / 1.60934,
					route.length / 1.60934
					).as_bytes())
				.expect(format!("Failed to header to manifest file. {}", manifest_out).as_str());
		});
	}
}

fn route(pt1: &Waypoint, pt2: &Waypoint, routino_profile: &str, routino_db: &str, towers: &Vec::<Tower>) -> Route{
	let routino_db_vec = routino_db.split("/").collect::<Vec<_>>();

	let cmd = format!(
		"router --lat1={} --lon1={} --lat2={} --lon2={} --output-stdout --profile={} --dir={} --prefix={} --quickest --output-text-all",
		pt1.lat,
		pt1.lon,
		pt2.lat,
		pt2.lon,
		routino_profile,
		&routino_db_vec[0..routino_db_vec.len() - 1].join("/"),
		routino_db_vec[routino_db_vec.len() - 1].replace("-nodes.mem", "")
	);

	let output: std::process::Output = if cfg!(target_os = "windows") {
		panic!("I don't know batch commands, sorry.")
	} else {
		Command::new("sh").arg("-c").arg(&cmd).output()
			.expect(format!("Failed to list contents of cell profile cache. CMD: {}", &cmd).as_str())
	};
	
	let ers = std::str::from_utf8(&output.stderr).expect("Failed to convert output of command.");
	if !ers.trim().is_empty() { panic!("cmd erred: cmd:{} err:{} pt1:{:?} pt2:{:?}", cmd, ers, pt1, pt2); }

	let mut total_length: f32 = 0.0;

	let mut root = 	Route {
		name: format!("{} - {}", pt1.name, pt2.name),
		waypoints: Vec::<Waypoint>::new(),
		cell_coverage: 0.0,
		length: 0.0
	};

	root.waypoints.push(pt1.clone());

	std::str::from_utf8(&output.stdout)
		.expect("Failed to convert output of command.")
		.split("\n")
		.map(|x| x.to_string())
		.filter(|x| !x.starts_with("#") && !x.trim().is_empty())
		.map(|x| x.split("\t").map(|y| y.trim().to_string()).collect::<Vec<_>>()) //Convert string to vec of junctions fields
		.for_each(|x| {
			let length = x[6].chars().filter(|c|c.is_numeric()||c==&'.').collect::<String>().parse::<f32>()
    				.expect(format!("Total length in routino response not a float. Val: {}", x[6]).as_str());

    		if length > total_length { total_length = length; }

			root.waypoints.push(Waypoint {
				lat: x[0].to_string().parse::<f32>()
	    				.expect(format!("Lat values in routino response not a float. Val: {}", x[0]).as_str()),
				lon: x[1].to_string().parse::<f32>()
	    				.expect(format!("Lat values in routino response not a float. Val: {}", x[1]).as_str()),
	    		desc: "".to_string(),
	    		name: "".to_string(),
	    		nearest_tower: 0.0
			});
		});


	root.waypoints.push(pt2.clone());

	root.waypoints.iter_mut().for_each(|x| {
		x.nearest_tower = compress_cell_db::nearest_tower(&towers, &x);
	});

	root.cell_coverage = 
		(root.waypoints.iter().filter(|x| x.nearest_tower / 1.60934 < 5.0).count() as f32 / 
		root.waypoints.iter().count() as f32) as f32;
	root.length = total_length;

	return root;
}
