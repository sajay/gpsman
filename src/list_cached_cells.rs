use std::process::Command;

pub fn list_cached_cells() -> Vec::<String> {
	let output: std::process::Output = if cfg!(target_os = "windows") {
		Command::new("cmd").args(["/C", "dir %USER%/gpxman.cache"]).output().expect("failed to execute process")
	} else {
		Command::new("sh").arg("-c").arg("ls ~/.gpxman.cache/cell_profiles").output().expect("Failed to list contents of cell profile cache. CMD: ls ~/gpxman.cache/cell_profiles")
	};
	
	return std::str::from_utf8(&output.stdout)
		.expect("Failed to convert output of command.")
		.split("\n")
		.map(|x| x.to_string())
		.collect::<Vec<_>>();
}
