use geoutils::Location;

#[derive(Debug)]
pub struct Tower{
	pub lat: f32,
	pub lon: f32,
	pub samples: u64
}

pub trait TraitTower{
	fn dist(&self, other: &Tower) -> f32;
}
impl TraitTower for Tower{
	fn dist(&self, other: &Tower) -> f32 {
		let pt1 = Location::new(self.lat, self.lon);
		let pt2 = Location::new(other.lat, other.lon);
		return pt1.distance_to(&pt2).unwrap().meters() as f32 / 1000.0;
	}
}

#[derive(Debug, Clone, Default)]
pub struct Waypoint{
	pub lat: f32,
	pub lon: f32,
	pub name: String,
	pub desc: String,
	pub nearest_tower: f32, //km
}
pub trait TraitWaypoint{
        fn dist(&self, other: &Waypoint) -> f32;
}
impl TraitWaypoint for Waypoint{
        fn dist(&self, other: &Waypoint) -> f32 {
                let pt1 = Location::new(self.lat, self.lon);
                let pt2 = Location::new(other.lat, other.lon);
                return pt1.distance_to(&pt2).unwrap().meters() as f32 / 1000.0;
        }
}



#[derive(Debug, Default)]
pub struct Route{
	pub name: String,
	pub waypoints: Vec::<Waypoint>,
	pub length: f32, //km
	pub cell_coverage: f32, //Percentage of route within 10km of a tower
}
