//use rayon::prelude::*;
use std::{io::{BufReader, BufRead, Write}, fs::{File} };
use crate::objects::*;
use xmltree::{Element, XMLNode};

pub fn export_route_to_device(ifile: &str, dirname: &str, ext: &str){
    let ifile = std::fs::read_to_string(ifile).expect(format!("Failed to open gpx route file for reading: {}", ifile).as_str());
	let root = Element::parse(ifile.as_bytes()).unwrap();
 	
 	let _ = std::fs::rename(dirname, format!("{}.old", dirname));

 	std::fs::create_dir(dirname).expect(format!("Failed to create output dir: {}", dirname).as_str());

 	let mut i: u64 = 0;
 	root.children.iter().for_each(|n| {
 		match n {
 			XMLNode::Element(e) => {
 				if e.name == "rte" || e.name == "trk" {
			 		let filename = format!("{}/{:0>3}.{}", dirname, i, ext);
			 		let mut file_element = Element::new("gpx");
			 		file_element.children.push(n.clone());

				 	file_element.write_with_config(
				 		std::fs::File::create(&filename)
				 		.expect(format!("Failed to write clean xml to gpx file. {}", &filename).as_str()),
				 		xmltree::EmitterConfig::new().line_separator("\n").perform_indent(true)
				 		).expect(format!("Failed to write clean xml to gpx file. {}", &filename).as_str());
				 	i += 1;
		 		}
		 	},
		 	_ => {}
		}
 	});
}
