use xmltree::{Element, XMLNode};
use std::fs;
use regex::Regex;

pub fn cleangpx(filename: &str) {
	let ifile = fs::read_to_string(filename).expect(format!("Failed to open gpx route file for reading: {}", filename).as_str());
	let root = XMLNode::Element(Element::parse(ifile.as_bytes()).unwrap());

	let root = recursive_remove_children(root);

 	let ofile = fs::read_to_string(filename).expect(format!("Failed to open gpx route file for reading: {}", filename).as_str());
 	
	std::fs::rename(filename, format!("{}.bak", filename)).expect(format!("Failed to backup file: {}", filename).as_str());

 	root.as_element().expect("Internal error").write_with_config(
 		fs::File::create(filename).expect(format!("Failed to write clean xml to gpx file. {}", filename).as_str()),
 		xmltree::EmitterConfig::new().line_separator("\n").perform_indent(true)
 		)
 		.expect(format!("Failed to write clean xml to gpx file. {}", filename).as_str());
 		
 	std::fs::remove_file(format!("{}.bak", filename)).expect(format!("Failed to backup file: {}", filename).as_str());
}

fn recursive_remove_children(mut root: XMLNode) -> XMLNode {
	match root {
		XMLNode::Element(ref mut e1) => {
			e1.namespace = None;
			e1.namespaces = None;
			e1.children = e1.children.iter().filter(|node| {
		 		match node {
		 			XMLNode::Element(e) => { 
		 				static BAD_NODES: [&str; 5] = [ "history", "WaypointExtension", "metadata", "time", "extensions" ];
		 				return !BAD_NODES.contains(&e.name.as_str());
		 			},
		 			_ => true
		 		}
		 	})
		 	.map(|x|
		 		recursive_remove_children(x.clone())
		 	).collect::<Vec<_>>();
		 	root
		},
		XMLNode::Text(ref mut t) => { *t = clean_string(t); root },
		_ => root
	}
}

fn clean_string(s: &String) -> String {
	let t0 = Regex::new(r"<[\w|/]+?.*?>").unwrap().replace_all(s, "").to_string();
	return t0.trim().to_string();
}