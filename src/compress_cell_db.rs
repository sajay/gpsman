use rayon::prelude::*;
use std::{io::{BufReader, BufRead, Write}, fs::{File} };
use crate::objects::*;

pub fn compress_cell_db(input_file: &str, mncs: Vec::<u16>, mccs: Vec::<u16>, radius: f32) -> Vec::<Tower> {
	let mut all_towers: Vec<Tower> = Vec::<Tower>::with_capacity(10000000);

    let file = File::open(&input_file).expect(format!("Failed to open input file: {}", input_file).as_str());
  
    let reader = BufReader::new(file);
    let mut total: u64 = 0;
	
    for line in reader.lines() {
        let tower_details: Vec<&str> = line.as_ref().expect("Bad line in file").split(",").collect::<Vec<_>>();
        total += 1;
        if tower_details[0] == "LTE"
        	&& mccs.contains(&tower_details[1].parse::<u16>().expect("MCC not an int"))
        	&& mncs.contains(&tower_details[2].parse::<u16>().expect("MNC not an int"))
        	&& tower_details[9].parse::<u32>().expect("samples not an int") > 1	{
        	
			let tmp: Tower = Tower { 
				lat: tower_details[7].parse::<f32>().expect("lat not a float"),
				lon: tower_details[6].parse::<f32>().expect("lon not a float"),
				samples: tower_details[9].parse::<u64>().expect("samples not an int")
			};
        	
        	if !all_towers.par_iter().any(|t| t.dist(&tmp) < radius ) {
        		all_towers.push(tmp);
        		if all_towers.len() % 100 == 0{ println!("{}%", (total) as f32 / 454485.03) }
        	}
        }
    }
    return all_towers;
}

pub fn load_cell_profile(input_file: &str) -> Vec::<Tower> {
    let mut all_towers: Vec<Tower> = Vec::<Tower>::with_capacity(10000000);

    let file = File::open(&input_file).expect(format!("Failed to open input file: {}", input_file).as_str());
  
    let reader = BufReader::new(file);
    
    for line in reader.lines() {
        let ln = line.as_ref().expect("Bad line in cell profile file");

        if !ln.trim().is_empty() {

            let tower_details: Vec<&str> = ln.split(",").collect::<Vec<_>>();

            let tmp: Tower = Tower { 
                lat: tower_details[0].parse::<f32>().expect("lat not a float"),
                lon: tower_details[1].parse::<f32>().expect("lon not a float"),
                samples: tower_details[2].parse::<u64>().expect("samples not an int")
            };
            
            all_towers.push(tmp);
        }
    }
    return all_towers;
}

pub fn nearest_tower(towers: &Vec::<Tower>, pt: &Waypoint) -> f32 {
    let psudo_tower = &Tower { lat: pt.lat, lon: pt.lon, samples: 0 };
    let mut min_dist = f32::MAX;

    towers.iter().for_each(|x| {
        if x.dist(psudo_tower) < min_dist {
            min_dist = x.dist(psudo_tower);
        }
    });
    return min_dist;
}