build:
	cargo build

install:
	sudo chown root ./target/debug/gpxman
	sudo chgrp root ./target/debug/gpxman
	sudo chmod 755 ./target/debug/gpxman
	sudo cp ./target/debug/gpxman /bin/
	
